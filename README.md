# RSSfeeds: is a feeds (rss, atom, tweeter, ...) server

# Usage

Basically rssfeeds provides an api to manage feeds. Update of feeds is in another [application](http://bitbucket.org/pierreaubert/crawler). Users feeds ares in again another [application](http://bitbucket.org/pierreaubert/userfeeds).

# Primer

First you need to be authenticated thus send a POST request to:
```
http://127.0.0.1/login
```
with a json payload
```
{ "username": "tester", "password": "simple" }
```

## Feeds API

### Read feeds
This work as expected with calls to */feeds*
```
GET /feeds
```
will return a list of feeds, something like:
```
{
    "feeds": [
        {
            "blacklisted": null,
            "compute_language": "??",
            "compute_nb_items": 0,
            "error_count": 2,
            "favicon": null,
            "http_etag": null,
            "http_status": 304,
            "last_checked": "Mon, 21 Jul 2014 12:13:36 GMT",
            "last_modified": "Fri, 30 May 2014 20:11:26 GMT",
            "metadata_update": 1,
            "rssfeed_id": 487,
            "subscribers": 0,
            "title": "NO TICKET REQUIRED — The Veritix Blog » tennis",
            "update_frequency": 1,
            "url": "http://blog.veritix.com/?feed=rss2&tag=tennis\n"
        },
        {
            "blacklisted": null,
            "compute_language": "??",
            "compute_nb_items": 0,
            "error_count": 0,
            "favicon": null,
            "http_etag": null,
            "http_status": 200,
            "last_checked": "Mon, 21 Jul 2014 12:13:36 GMT",
            "last_modified": "Mon, 21 Jul 2014 13:09:02 GMT",
            "metadata_update": 1,
            "rssfeed_id": 485,
            "subscribers": 0,
            "title": "Comments on: iQ",
            "update_frequency": 1,
            "url": "http://blog.toyota.co.uk/range/iq/feed\n"
        }
}
```

### Read one feed
Feeds are identified with an integer
```
GET /feeds/487
```
will return a list of feeds, something like:
```
{
    "feeds": [
        {
            "blacklisted": null,
            "compute_language": "??",
            "compute_nb_items": 0,
            "error_count": 2,
            "favicon": null,
            "http_etag": null,
            "http_status": 304,
            "last_checked": "Mon, 21 Jul 2014 12:13:36 GMT",
            "last_modified": "Fri, 30 May 2014 20:11:26 GMT",
            "metadata_update": 1,
            "rssfeed_id": 487,
            "subscribers": 0,
            "title": "NO TICKET REQUIRED — The Veritix Blog » tennis",
            "update_frequency": 1,
            "url": "http://blog.veritix.com/?feed=rss2&tag=tennis\n"
        }
}
```

### Read items associated with one feed
Feeds are identified with an integer
```
GET /feeditems/487
```
will return a list of feeds, something like:
```
{
    "feed_id": 487,
    "items": [
        {
            "content": "Veritix is pleased to announce that it has been selected to provide ticketing services for the 2011 World TeamTennis Smash Hits presented by GEICO.  The one-night all-star tennis event will be held on Thursday, October 27 at Cleveland Public Hall and feature legendary tennis stars such as Andy Roddick, Martina Navratilova, and John McEnroe to name a few.  All proceeds from the event will benefit the Elton John AIDS Foundation and the AIDS Taskforce of Greater Cleveland.\n“WTT Smash Hits has long been a successful fundraiser for the Elton John AIDS Foundation and local AIDS organizations in our host cities. This year we are pleased to bring the event to Cleveland for the first time ever and to help benefit the AIDS Taskforce of Greater Cleveland,” said Kerry Schneider, Director of Marketing and Sponsorship Services for World TeamTennis.  ”Partnering with Veritix for our ticketing services provides convenience for our donors and patrons, making it easier than ever to obtain tickets and support this worthy cause.”\nClick here to read the full press release.\n \n ",
            "description": "Veritix is pleased to announce that it has been selected to provide ticketing services for the 2011 World TeamTennis Smash Hits presented by GEICO.  The one-night all-star tennis event will be held on Thursday, October 27 at Cleveland Public Hall and feature legendary tennis stars such as Andy Roddick, Martina Navratilova, and John McEnroe to [&#8230;]",
            "link": "http://blog.veritix.com/index.php/veritix-to-provide-ticketing-services-for-2011-world-teamtennis-smash-hits-presented-by-geico/",
            "published": "Wed, 05 Oct 2011 14:15:05 GMT",
            "remote_id": null,
            "rssfeed_id": 487,
            "rssitem_id": 1939,
            "title": "Veritix® to Provide Ticketing Services for 2011 World TeamTennis Smash Hits Presented by GEICO",
            "updated": "Wed, 05 Oct 2011 14:15:05 GMT"
        }
    ],
    "status": "ok"
}
```

### Add a new feed
```
POST /feeds
```

### Modify a feed
You must have admin right to modify a feed
```
PUT /feeds
```

### Remove a feed
You must have admin right to remove a feed
```
DELETE /feeds/437
```


## Items API

To add items
```
POST /items
```
and to modify item with *id* them
```
PUT /items/id
```
and to delete item with *id* them
```
DELETE /items/id
```

## Search API

### Search feeds by *url*
```
GET /search/url?url=url
```

### Search feeds matching *keywords* in feed description, title or url
```
GET /search/feeds/kw1+kw2
```

### Search feeds matching *keywords* in content
```
GET /search/items/kw1+kw2
```


## Crawl API
This API is used to know which feeds to refresh
```
GET /crawl/command
```
where *command* can be
* *new*: feeds which have been added and never crawled
* *error*: feeds which have been crawler before but there was errors (http, dns, parsing, ...) last time
* *oldest*: feeds which have not been crawled in a long time
* *refresh*: feeds to refresh
* *withoutlanguage*: feeds where the analyser didn't deduce a language automatically


```k# And now

Full documentation is in the [rssfeeds wiki](http://bitbucket.org/pierreaubert/rssfeeds/wiki).

Thanks for reading this far.

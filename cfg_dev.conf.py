#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from os import environ
import logging
from datetime import date


# version
VERSION = "0.07-dev"
# secret key
SECRET_KEY='dev&debug&are&fun'
# debug is false by default
DEBUG = False
# testing is false by default
TESTING = False
# your favorite database, sqlalchimy convention
HOME='{0}/tmp'.format(environ['HOME'])
#DATABASE_URI = 'sqlite://:memory'
#DATABASE_URI = 'sqlite:////{0}/rssfeeds_dev.db'.format(HOME)
DATABASE_URI='postgresql+psycopg2://rssfeeds:gargamellekkksso@localhost/rssfeeds'
# main log file
LOG_APP =  "rssfeeds_dev"
LOG_FILE= "{0}/{1}_{2}.log".format(HOME, LOG_APP, date.today().isoformat())
LOG_LEVEL= logging.INFO
# host/port for techradar
RSSFEEDS_PORT=9081
RSSFEEDS_HOST="0.0.0.0"
RSSFEEDS_URL='http://{}:{}'.format(RSSFEEDS_HOST, RSSFEEDS_PORT)
# host/port for sso
SSO_PORT=9080
SSO_HOST="0.0.0.0"
SSO_URL='http://{}:{}'.format(SSO_HOST, SSO_PORT)
SSO_ADMIN_USERNAME='admin'
SSO_ADMIN_PASSWORD='admin2013'
# do we use sentry
USE_SENTRY=False
SENTRY_DSN=''
#SENTRY_DSN='http://3eed676897fc45c3a1f4ff700fffe339:7195ff5b79314928a4f68632c1a88789@sentry.7pi.eu:9000/3'
# set up a few users:
CRAWLER_USERNAME='crawler'
CRAWLER_PASSWORD='crawler2013'
CRAWLER_EMAIL='crawler@localhost'





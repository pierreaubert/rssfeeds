#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from os import environ
import logging
from datetime import date

# version
VERSION = "0.07-test"
# secret key
SECRET_KEY='dev&debug&are&fun'
# debug is false by default
DEBUG = True
# testing is false by default
TESTING = True
# your favorite database, sqlalchimy convention
HOME='{0}/tmp'.format(environ['HOME'])
#DATABASE_URI = 'sqlite://:memory'
DATABASE_URI = 'sqlite:////{0}/rssfeeds_test.db'.format(HOME)
# main log file
LOG_APP =  "rssfeeds_test"
LOG_FILE= "{0}/{1}_{2}.log".format(HOME, LOG_APP, date.today().isoformat())
LOG_LEVEL= logging.ERROR
# host/port for rssfeeds and sso
RSSFEEDS_PORT=9091
RSSFEEDS_HOST="0.0.0.0"
# host/port for sso (not used in test)
# sso is faked
SSO_URL='http://127.0.0.1:9090'
SSO_ADMIN_USERNAME='admin'
SSO_ADMIN_PASSWORD='admin2013'
# do we use sentry
USE_SENTRY=False
SENTRY_DSN=''
#SENTRY_DSN='http://4090c55ae7754c6ca0bc5aed20538c88:0a309b08f418447f9e1c275028c7f1e2@192.168.1.9:9000/2'
# set up a few users:
CRAWLER_USERNAME='crawler'
CRAWLER_PASSWORD='crawler2013'
CRAWLER_EMAIL='crawler@localhost'








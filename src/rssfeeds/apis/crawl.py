#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from flask import current_app, request, jsonify
from flask.views import MethodView

from sso.accesscontrol.ac import AC_User
from sso.accesscontrol.exceptions import BadRequest, NotFound
from sso.client.decorators.dflask import allow
from sso.helpers import get_limit

from rssfeeds.models.feeds import RssFeeds
from helpers import rssfeeds_db_get_conn


""" list of command understood in the rssfeeds.models"""
_crawl_command = [
    'new',                # return new feeds
    'error',              # return feeds in a error state
    'oldest',             # return oldest feeds to refresh
    'refresh',            # return feeds to refresh
    'withoutlanguage',    # return feeds without a computed lang
]


class CrawlAPI(MethodView):
    """
    # ------------------------------------------------------------------
    # GET    /crawl/new       return a list of new feeds
    # GET    /crawl/error     return a list of feeds which have been in
    #                         error before
    # GET    /crawl/refresh   return a list of feeds to refresh
    # GET    /crawl/withoutlanguage
    #                         return a list of feeds to without a lang
    # ------------------------------------------------------------------
    """
    @allow(AC_User('crawler'))
    def get(self, command):
        db_conn = rssfeeds_db_get_conn()
        limit = get_limit(request)
        feeds = None

        #print 'command: '+command
        if command not in _crawl_command:
            return BadRequest('unkown command: '+command)

        feeds = RssFeeds(current_app.logger, db_conn).crawl(command, limit)
        if feeds is None:
            raise NotFound('no feed')

        resp = {
            'status': 'ok',
            'feeds': feeds
        }
        return jsonify(resp)

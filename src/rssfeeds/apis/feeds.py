#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from flask import current_app, request, jsonify
from flask.views import MethodView

from sso.client.decorators.dflask import authenticate, allow
from sso.accesscontrol.exceptions import BadRequest, NotFound
from sso.accesscontrol.ac import AC_User
from sso.helpers import get_limit, get_offset

from rssfeeds.models.feeds import RssFeeds
from rssfeeds.models.items import RssItems
from helpers import rssfeeds_db_get_conn


""" list of command understood in the models"""
_rssfeeds_command = [
    'ok',                 # code 200
    'error',              # code 200 but error in content
    'redirect',           # code 300, 301, 302, 303, 305, 307
    'deleted',            # code 400, 401, 402, 403, 404
    'updatelastchecked',  # code 304
    'ko',                 # code 500, 502, 503, 504
]


class RssFeedsAPI(MethodView):
    """
    # ------------------------------------------------------------------
    # GET    /feeds           return a list of feeds
    # POST   /feeds           create a feed via a url (must be unique)
    # GET    /feeds/<feed_id> return data on feed (id)
    # PUT    /feeds/<feed_id> modify a feed
    # PATCH  /feeds/<feed_id> idem
    # DELETE /feeds/<feed_id> delete a feed by id
    #
    # GET    /feeditems/<feed_id> return items from feed
    # ------------------------------------------------------------------
    """
    def _get_one(self, feed_id):
        db_conn = rssfeeds_db_get_conn()
        feed = RssFeeds(current_app.logger, db_conn).get_one(feed_id)
        if feed is None:
            raise NotFound('unknown feed')
        resp = {
            'status': 'ok',
            'feed': feed
        }
        return jsonify(resp)

    def _get_many(self):
        db_conn = rssfeeds_db_get_conn()
        limit = get_limit(request)
        offset = get_offset(request)
        feeds = RssFeeds(current_app.logger, db_conn).get_many(offset, limit)
        if feeds is None:
            raise NotFound('unknown feed')
        resp = {
            'status': 'ok',
            'feeds': feeds
        }
        return jsonify(resp)

    def _get_items(self, feed_id):
        db_conn = rssfeeds_db_get_conn()
        limit = get_limit(request)
        offset = get_offset(request)
        items = RssItems(current_app.logger, db_conn).get_from_feed(feed_id, offset, limit)
        resp = {
            'status': 'ok',
            'feed_id': feed_id,
            'items': items
        }
        return jsonify(resp)

    @authenticate
    def get(self, feed_id=None):
        """ return feed feed_id """
        if feed_id is None:
            return self._get_many()
        elif '/feeditems' in request.path:
            return self._get_items(feed_id)
        else:
            return self._get_one(feed_id)

    @authenticate
    def post(self):
        """ add a new feed """
        db_conn = rssfeeds_db_get_conn()
        data = request.get_json()
        # print data
        if 'url' not in data and 'urls' not in data:
            raise BadRequest('url is required to add a feed')

        rss = RssFeeds(current_app.logger, db_conn)
        error = rss.post(data)
        resp = {}
        if error is None:
            resp['status'] = 'ok'
            resp['rssfeed_id'] = rss.lastrowid
        else:
            resp['status'] = 'ko'
            resp['error'] = error
        return jsonify(resp)

    @allow(AC_User('crawler'))
    def put(self, feed_id):
        """ update a feed """
        db_conn = rssfeeds_db_get_conn()
        data = request.get_json()
        command = request.args.get('command')
        if command is None:
            command = 'ok'
        elif command not in _rssfeeds_command:
            raise BadRequest('unkown command: {0}'.format(command))
        resp = {}
        rss = RssFeeds(current_app.logger, db_conn)
        error = rss.put(feed_id, command, data)
        if error is None:
            resp['status'] = 'ok'
            resp['rssfeed_id'] = rss.lastrowid
        else:
            resp['status'] = 'ko'
            resp['error'] = error
        return jsonify(resp)

    @allow(AC_User('crawler'))
    def patch(self, feed_id):
        """ update a feed """
        db_conn = rssfeeds_db_get_conn()
        data = request.get_json()
        rss = RssFeeds(current_app.logger, db_conn)
        command = request.args.get('command')
        if command is None:
            command = 'ok'
        elif command not in _rssfeeds_command:
            raise BadRequest('unkown command: '+command)
        error = rss.patch(feed_id, command, data)
        resp = {}
        if error is None:
            resp['status'] = 'ok'
            resp['rssfeed_id'] = rss.lastrowid
        else:
            resp['status'] = 'ko'
            resp['error'] = error
        return jsonify(resp)

    @allow(AC_User('admin'))
    def delete(self, feed_id):
        """ delete a feed
        TODO: check ON DELETE CASCADE in items
        """
        error = None
        db_conn = rssfeeds_db_get_conn()
        rss = RssFeeds(current_app.logger, db_conn)
        error = rss.delete(feed_id)
        resp = {}
        if error is None:
            resp['status'] = 'ok'
            resp['rssfeed_id'] = rss.lastrowid
        else:
            resp['status'] = 'ko'
            resp['error'] = error
        return jsonify(resp)

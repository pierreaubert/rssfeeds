#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from flask import g, current_app


def rssfeeds_db_get_conn():
    """ get a connection from pool and store it in global g
    """
    conn = getattr(g, 'rssfeeds_conn', None)
    if conn is None:
        #print '(trace) > create conn'
        conn = g.conn = current_app.rssfeeds_db.connect()
    #else:
    #    print '(trace) > reuse conn'
    return conn

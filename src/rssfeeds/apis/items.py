#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from flask import current_app, request, jsonify
from flask.views import MethodView

from sso.client.decorators.dflask import authenticate, allow
from sso.accesscontrol.ac import AC_User
from sso.accesscontrol.exceptions import NotFound, BadRequest

from rssfeeds.models.items import RssItems
from helpers import rssfeeds_db_get_conn


class RssItemsAPI(MethodView):
    """
    # ------------------------------------------------------------------
    # POST   /items           create a item attached to a feed
    # GET    /items/<item_id> return data on item (id)
    # PUT    /items/<item_id> modify a patch
    # PATCH  /items/<item_id> idem
    # DELETE /items/<item_id> delete a item by id
    # ------------------------------------------------------------------
    """
    @authenticate
    def get(self, rssitem_id):
        """ return item rssitem_id """
        db_conn = rssfeeds_db_get_conn()
        item = RssItems(current_app.logger, db_conn).get(rssitem_id)
        if item is None:
            raise NotFound()

        resp = {
            'status': 'ok',
            'item': item
        }
        return jsonify(resp)

    @allow(AC_User('crawler'))
    def post(self):
        """ add a new item """
        db_conn = rssfeeds_db_get_conn()
        data = request.get_json()
        if 'rssfeed_id' not in data:
            raise BadRequest('rssfeed_id is mandatory')

        feed_id = data['rssfeed_id']
        if feed_id is None:
            raise BadRequest('rssfeed_id is mandatory and cannot be Null')
        rss = RssItems(current_app.logger, db_conn)
        error = rss.post(feed_id, data)
        if error is None:
            resp = {
                'status': 'ok',
                'rssitem_id': rss.lastrowid
            }
            return jsonify(resp)
        return BadRequest(error)

    @allow(AC_User('crawler'))
    def put(self, rssitem_id):
        """ add a new item """
        db_conn = rssfeeds_db_get_conn()
        data = request.get_json()
        rss = RssItems(current_app.logger, db_conn)
        error = rss.put(rssitem_id, data)
        if error is None:
            resp = {
                'status': 'ok',
                'rssitem_id': rss.lastrowid
            }
            return jsonify(resp)
        return BadRequest(error)

    @allow(AC_User('crawler'))
    def patch(self, rssitem_id):
        """ add a new item """
        return self.put(rssitem_id)

    #@allow(AC_UserGroup('admin'))
    @allow(AC_User('admin'))
    def delete(self, rssitem_id):
        """ delete an item """
        db_conn = rssfeeds_db_get_conn()
        rss = RssItems(current_app.logger, db_conn)
        error = rss.delete(rssitem_id)
        if error is None:
            resp = {
                'status': 'ok',
            }
            return jsonify(resp)
        return BadRequest(error)

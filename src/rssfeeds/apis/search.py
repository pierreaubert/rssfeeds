#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from flask import current_app, request, jsonify
from flask.views import MethodView

from sso.client.decorators.dflask import authenticate
import sso.accesscontrol as ac

from rssfeeds.models.feeds import RssFeeds
from rssfeeds.models.items import RssItems
from rssfeeds.models.search import RssSearch

from helpers import rssfeeds_db_get_conn


class SearchUrlsAPI(MethodView):
    """
    # ------------------------------------------------------------------
    # GET /search/url?url=url search by url
    # ------------------------------------------------------------------
    """
    @authenticate
    def get(self):
        """ search by ur
        """
        db_conn = rssfeeds_db_get_conn()
        if 'url' not in request.args:
            raise ac.BadRequest('url is mendatory')
        urld = request.args['url']
        feed = RssFeeds(current_app.logger, db_conn).search_url(urld)
        if feed is None:
            raise ac.NotFound('unknown feed')
        resp = {
            'status': 'ok',
            'feed': feed
        }
        return jsonify(resp)


class SearchFeedsAPI(MethodView):
    """
    # ------------------------------------------------------------------
    # GET /search/feeds/<kw> search by keywords
    # ------------------------------------------------------------------
    """
    @authenticate
    def get(self, kw):
        """ 2 ways to search a feed
        """
        db_conn = rssfeeds_db_get_conn()
        ckw = kw.encode('unicode', 'ignore')
        feeds = RssSearch(current_app.logger, db_conn).search_feeds(ckw)
        resp = {
            'status': 'ok',
            'feeds': feeds
        }
        return jsonify(resp)


class SearchItemsAPI(MethodView):
    """
    # ------------------------------------------------------------------
    # GET /search/items/<kw> search by keywords
    # ------------------------------------------------------------------
    """
    @authenticate
    def get(self, kw):
        """ 2 ways to search a feed
        """
        db_conn = rssfeeds_db_get_conn()
        ckw = kw.encode('unicode', 'ignore')
        items = RssSearch(current_app.logger, db_conn).search_items(ckw)
        resp = {
            'status': 'ok',
            'items': items
        }
        return jsonify(resp)

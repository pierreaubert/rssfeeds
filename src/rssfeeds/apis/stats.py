#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from flask import jsonify, current_app
from flask.views import MethodView

from sso.client.decorators.dflask import authenticate, allow
from sso.accesscontrol.ac import AC_User

from rssfeeds.models.stats import Stats
from helpers import rssfeeds_db_get_conn


class StatsAPI(MethodView):
    """ return statistics
    # ------------------------------------------------------------------
    # GET /stats     return a json with a distribution of datas
    # PATCH /stats   refresh the distributed data
    # TODO: must be cached, 'users' get access to cache version only
    # ------------------------------------------------------------------
    """
    @authenticate
    def get(self):
        """ return statistics
        """
        db_conn = rssfeeds_db_get_conn()
        stats = Stats(current_app.logger, db_conn).get()
        resp = {
            'status': 'ok',
            'stats': stats
        }
        return jsonify(resp)

    @allow(AC_User('admin'))
    def patch(self):
        """ compute statistics
        """
        db_conn = rssfeeds_db_get_conn()
        stats = Stats(current_app.logger, db_conn).compute()
        resp = {
            'status': 'ok',
            'stats': stats
        }
        return jsonify(resp)

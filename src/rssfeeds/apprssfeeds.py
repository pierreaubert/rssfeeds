#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
import logging
from flask import Flask, g
from sqlalchemy import create_engine

from sso.accesscontrol.exceptions import Unauthorized
from sso.client import rest as sso_client
from sso.client.decorators.dflask import forward_ssosign_cookie
from models.meta import rssfeeds_meta
from models.dbschema import db_schema_exist
from models.search import RssSearch
from apis.feeds import RssFeedsAPI
from apis.items import RssItemsAPI
from apis.stats import StatsAPI
from apis.crawl import CrawlAPI
from apis.search import SearchFeedsAPI, SearchUrlsAPI, SearchItemsAPI


def db_close_conn(exception):
    conn = getattr(g, 'rssfeeds_conn', None)
    if conn is not None:
        #print '(trace) > close conn'
        conn.close()
    #else:
    #    print '(trace) > already closed conn'


def db_check(logger, engine, db_uri):
    """ create the databases associated with ssi
    """
    if not db_schema_exist(engine):
        # create all tables via sqlachemy
        rssfeeds_meta.create_all(bind=engine)
        # for full text we have to do it ourself
        # we cannot use db_get_conn because it depends of app context
        search = RssSearch(logger, engine.connect(), db_uri)
        search.create_tables()

    # are we up to date
    #from alembic.config import Config
    #from alembic import command
    #alembic_cfg = Config("alembic.ini")
    #command.stamp(alembic_cfg, "head")


def create_user(app, logger, env):
    """ create user crawler if not exist """
    sso = sso_client.Client(app.config['SSO_URL'], logger)
    try:
        sso.post_login(env['CRAWLER_USERNAME'], 
                       env['CRAWLER_PASSWORD'])
    except Unauthorized:
        # ok didn't work
        # log as admin and create user crawler
        try:
            sso.post_login(env['SSO_ADMIN_USERNAME'], 
                           env['SSO_ADMIN_PASSWORD'])
        except Unauthorized:
            logger.error('Cannot connect as admin, fatal error')
            raise Exception('Cannot connect as admin, fatal error')
        logger.debug('Connected as admin')
        # log as admin ok    
        try:
            logger.info('Try to create user crawler')
            sso.post_profile(env['CRAWLER_USERNAME'], 
                             env['CRAWLER_PASSWORD'],
                             env['CRAWLER_EMAIL'])
        except Unauthorized as u:
            logger.error('Cannot create user {0}: {1}'.format(
                env['CRAWLER_USERNAME'], u))

            
def create_rssfeeds():
    # create application
    rssfeeds = Flask('rssfeeds')
    rssfeeds.config.from_envvar('RSSFEEDS_SETTINGS')

    # add a file logger
    rssfeeds.logger.setLevel(rssfeeds.config['LOG_LEVEL'])
    fh = logging.FileHandler(rssfeeds.config['LOG_FILE'])
    fm = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    fh.setFormatter(fm)
    rssfeeds.logger.addHandler(fh)

    # if using Sentry add a logger
    if len(rssfeeds.config['SENTRY_DSN']) > 0:
        # if you want to use Sentry
        from raven.contrib.flask import Sentry
        from raven.handlers.logging import SentryHandler

        sentry = Sentry(rssfeeds, dsn=rssfeeds.config['SENTRY_DSN'])
        sentry.init_app(rssfeeds)
        sh = SentryHandler(rssfeeds.config['SENTRY_DSN'])
        rssfeeds.logger.addHandler(sh)

    # fire engine
    rssfeeds.rssfeeds_db = create_engine(rssfeeds.config['DATABASE_URI'])
    db_check(rssfeeds.logger, rssfeeds.rssfeeds_db, rssfeeds.config['DATABASE_URI'])

    # add routes /feeds
    rssfeeds_view = RssFeedsAPI.as_view('rssfeeds_api')
    rssfeeds.add_url_rule(
        '/feeds',
        view_func=rssfeeds_view,
        methods=['GET', 'POST'])
    rssfeeds.add_url_rule(
        '/feeds/<int:feed_id>',
        view_func=rssfeeds_view,
        methods=['GET', 'PUT', 'PATCH', 'DELETE'])

    rssfeeds.add_url_rule(
        '/feeditems/<int:feed_id>',
        view_func=rssfeeds_view,
        methods=['GET'])

    # add routes /items
    rssitems_view = RssItemsAPI.as_view('rssitems_api')
    rssfeeds.add_url_rule(
        '/items',
        view_func=rssitems_view,
        methods=['POST'])
    rssfeeds.add_url_rule(
        '/items/<int:rssitem_id>',
        view_func=rssitems_view,
        methods=['GET', 'PUT', 'PATCH', 'DELETE'])

    # add routes /crawl
    crawl_view = CrawlAPI.as_view('crawl_api')
    rssfeeds.add_url_rule(
        '/crawl/<string:command>',
        view_func=crawl_view,
        methods=['GET'])

    # add routes /stats
    stats_view = StatsAPI.as_view('stats_api')
    rssfeeds.add_url_rule(
        '/stats',
        view_func=stats_view,
        methods=['GET', 'PATCH'])

    # add routes /search/urls
    search_urls_view = SearchUrlsAPI.as_view('search_urls_api')
    rssfeeds.add_url_rule(
        '/search/urls',
        view_func=search_urls_view,
        methods=['GET'])

    # add routes /search/feeds
    search_feeds_view = SearchFeedsAPI.as_view('search_feeds_api')
    rssfeeds.add_url_rule(
        '/search/feeds',
        view_func=search_feeds_view,
        methods=['GET'])

    # add routes /search/items
    search_items_view = SearchItemsAPI.as_view('search_items_api')
    rssfeeds.add_url_rule(
        '/search/items',
        view_func=search_items_view,
        methods=['GET'])

    # add @rssfeeds.teardown_appcontext
    rssfeeds.teardown_appcontext(db_close_conn)

    # add @rssfeeds.before_request
    # rssfeeds.before_request(cache_profile_in_g)

    # add @rssfeeds.after_request
    rssfeeds.after_request(forward_ssosign_cookie)

    # create a crawler user if needed
    create_user(rssfeeds, rssfeeds.logger, rssfeeds.config )

    return rssfeeds


# create app
app = create_rssfeeds()

if __name__ == '__main__':
    app.run(host=app.config['RSSFEEDS_HOST'],
            port=app.config['RSSFEEDS_PORT'],
            debug=app.config['DEBUG'])


# because sometimes you have a stupid mix of keyboards/locale/os
# [] {} / \

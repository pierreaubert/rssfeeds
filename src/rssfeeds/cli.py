#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import logging
import argparse
import sys
from sso import client as sso_rest
import client as rssfeeds_rest


# create a empty namespace
class Command(object):
    def __init__(self):
        pass
command = Command()

# create a logger
logger = logging.getLogger(__name__)

# create a sso client and a rssfeeds client
sso_url = 'http://localhost:8081'
sso_client = sso_rest.Client(sso_url, logger)
rssfeeds_url = 'http://localhost:8080'
rssfeeds_client = rssfeeds_rest.Client(rssfeeds_url, logger,
                                       sso_client.session)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='cli to rssfeeds server')
    parser.add_argument('-u', '--username',
                        type=str,
                        help='username')
    parser.add_argument('-p', '--password',
                        type=str,
                        help='password')
    parser.add_argument('-c', '--config',
                        type=str,
                        help='config file')
    args = parser.parse_args(namespace=command)

    r = sso_client.post_login(args.username, args.password)
    if r is not None:
        print r
        sys.exit(1)

    sys.exit(0)

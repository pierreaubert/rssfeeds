#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import json
import requests
from sso.accesscontrol.exceptions import BadRequest


class Client():
    """ pure rest client for rssfeeds server
    TODO: be consistent in answers
    """

    def __init__(self, rssfeeds_url, sso_session, logger):
        """ initialise the client with traditionnal parameters
        sso_session is a session from requests
        (it holds the authent cookie)

        :param rssfeeds_url url of the server
        :param sso_session the session is taken care of by requests
        :param logger the logger for this application
        """
        self.rssfeeds_url = rssfeeds_url
        self.logger = logger
        self.session = sso_session

    def feed_post(self, url, title=None):
        """ create a feed in database
        return +1 if successfully posted
        return  0 if posted but already here
        return -1 if not posted correctly
        """
        data_feed = {
            'url': url,
        }
        if title is not None:
            data_feed['title'] = title
        r = self.session.post(self.rssfeeds_url+'/feeds',
                              data=json.dumps(data_feed))
        if r.status_code == requests.codes.ok:
            status = r.json()
            if 'status' in status and status['status'] == 'ok':
                return 1
            else:
                return 0
        else:
            self.logger.error('Cannot post feed error {0}'.format(r.status_code))
            return -1

    def feed_put(self, command, data_feed):
        """
        apply a command to a feed
        list of available command is describe in apis.py

        return +1 if successfully posted
        return  0 if posted but already here
        return -1 if not posted correctly
        """
        data_feed['command'] = command
        if 'rssfeed_id' not in data_feed:
            raise BadRequest('rssfeed_id is missing')
        r = self.session.put(self.rssfeeds_url +
                             '/feeds/{0}?command={1}'
                             .format(data_feed['rssfeed_id'], command),
                             data=json.dumps(data_feed))
        if r.status_code == requests.codes.ok:
            status = r.json()
            if 'status' in status and status['status'] == 'ok':
                return 1
            else:
                return 0
        else:
            return -1

    def feed_get_id(self, url):
        """ return the feed_id for a given url
        return -1 if not found
        """
        furl = self.rssfeeds_url+'/search/urls/'
        params = {
            'url': url,
        }
        r = self.session.get(furl, params=params)
        if r.status_code == requests.codes.ok:
            response = r.json()
            if 'status' in response and \
               response['status'] == 'ok' and \
               'feed' in response and \
               'rssfeed_id' in response['feed']:
                rssfeed_id = response['feed']['rssfeed_id']
                return rssfeed_id
        return -1

    def feed_get(self, id):
        """ return a dict with datas from this feed
        :param id
        """
        furl = self.rssfeeds_url+'/feeds/{0}'.format(id)
        r = self.session.get(furl)
        if r.status_code == requests.codes.ok:
            status = r.json()
            if len(status) >= 2:
                return status['feed']
            else:
                self.logger.error('client failed len(status) is {0}: status is {1}'
                                  .format(len(status), status))
        else:
            self.logger.error('request failed ({0}) {1}'
                              .format(r.status_code, furl))
        return dict()

    def feed_get_items(self, id):
        """ return a list of items from this feed
        :param id
        """
        furl = self.rssfeeds_url+'/feeditems/{0}'.format(id)
        r = self.session.get(furl)
        if r.status_code == requests.codes.ok:
            status = r.json()
            if len(status) >= 2:
                return status['items']
            else:
                self.logger.error('client failed len(status) is {0}: status is {1}'
                                  .format(len(status), status))
        else:
            self.logger.error('request failed ({0}) {1}'
                              .format(r.status_code, furl))
        return dict()

    def item_get(self, item_id):
        """ return a dict with datas from this item
        :param item_id  item number
        """
        furl = self.rssfeeds_url+'/items/{0}'.format(item_id)
        r = self.session.get(furl)
        if r.status_code == requests.codes.ok:
            status = r.json()
            if len(status) > 0:
                return status
            else:
                self.logger.error('client failed')
        else:
            self.logger.error('request failed ({0}) {1}'
                              .format(r.status_code, furl))
        return dict()

    def item_post(self, item_feed):
        """ update an item in database
        return +1 if successfully posted
        return  0 if posted but already here
        return -1 if not posted correctly
        """
        r = self.session.post(self.rssfeeds_url+'/items',
                              data=json.dumps(item_feed))
        if r.status_code == requests.codes.ok:
            status = r.json()
            if 'status' in status and status['status'] == 'ok':
                return 1
            else:
                return 0
        else:
            if r.status_code == 400:
                status = r.json()
                # print status
            return -1

    def crawl(self, command, limit):
        """
        """
        r_url = self.rssfeeds_url+'/crawl/{0}?limit={1}'.format(command, limit)
        self.logger.debug('trying to call {0}'.format(r_url))
        r = self.session.get(r_url)
        if r.status_code == requests.codes.ok:
            status = r.json()
            if 'status' in status and status['status'] == 'ok':
                return status['feeds']
            else:
                self.logger.error('(http: {1}) no status after calling {0}'
                                  .format(r_url, r.status_code))
                return None
        else:
            self.logger.error('cannot connect to '+r_url)
        return None

#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from sqlalchemy import select
from sqlalchemy.exc import NoSuchTableError, \
    OperationalError, SQLAlchemyError

from feeds import rssfeeds as table_rssfeeds


def db_schema_exist(engine):
    """ return true is database exist and
    schema has been uploaded
    """
    s = select([table_rssfeeds])
    try:
        conn = engine.connect()
        conn.execute(s).fetchone()
        return True
    except NoSuchTableError:
        return False
    except OperationalError:
        return False
    except SQLAlchemyError:
        return False

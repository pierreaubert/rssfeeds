#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
import dateutil.parser
from datetime import datetime
from sqlalchemy import Table, Column, Integer, String, Text, Boolean, \
    UniqueConstraint, Sequence, Index, DateTime, select
from sqlalchemy.exc import IntegrityError, OperationalError
from sqlalchemy.sql.expression import desc

from sso.accesscontrol.exceptions import BadRequest
from sso.helpers import row2dict, rows2dict

from meta import rssfeeds_meta

rssfeeds = Table(
    'rssfeeds', rssfeeds_meta,
    Column('rssfeed_id', Integer, Sequence('rssfeeds_seq'), primary_key=True),
    Column('url', String(512), nullable=False),
    Column('favicon', String(512), nullable=True),
    Column('last_checked', DateTime, nullable=True),
    Column('subscribers', Integer, default=0),
    Column('title', Text),
    Column('update_frequency', Integer, default=0),
    Column('error_count', Integer, default=0),
    Column('metadata_update', Integer, default=1),
    Column('http_status', Integer, default=0),
    Column('http_etag', String(64), nullable=True),
    Column('last_modified', DateTime, nullable=True),
    Column('compute_language', String(2), default='??'),
    Column('compute_nb_items', Integer, default=0),
    Column('blacklisted', Boolean, nullable=True),
    UniqueConstraint('url', name='rssfeeds_url_uniq')
)


Index('rssfeeds_url_idx', rssfeeds.c.url, unique=True)


from redirect import httpredirects


class RssFeeds(object):
    """ Rssfeeds models a list of feed for a user
    # ------------------------------------------------------------------
    #
    # ------------------------------------------------------------------
    """
    def __init__(self, logger, conn):
        self.logger = logger
        self.conn = conn
        self.lastrowid = -1

    def get_one(self, feedid):
        s = select([rssfeeds]).where(rssfeeds.c.rssfeed_id == feedid)
        row = self.conn.execute(s).fetchone()
        if row is not None:
            return row2dict(s, row)
        return None

    def search_url(self, url):
        s = select([rssfeeds]).where(rssfeeds.c.url == url)
        row = self.conn.execute(s).fetchone()
        if row is not None:
            return row2dict(s, row)
        return None

    def get_many(self, offset, limit):
        s = select([rssfeeds]).limit(limit).offset(offset)
        rows = self.conn.execute(s).fetchall()
        if rows is not None:
            return rows2dict(s, rows)
        return None

    def _post_one(self, data):
        """ add a url (we expect the url to be encoded)
        """
        try:
            url = data['url']
            ins = rssfeeds.insert().values(url=url)
            row = self.conn.execute(ins)
            self.lastrowid = row.lastrowid
            return None
        except IntegrityError as e:
            shortmsg = 'feed already available'
            self.logger.debug(shortmsg+str(e))
            return shortmsg

    def _post_many(self, data):
        """ add a list of urls
        """
        try:
            ins = rssfeeds.insert()
            urls = [{'url': url} for url in data['urls']]
            row = self.conn.execute(ins, urls)
            self.lastrowid = row.lastrowid
            return None
        except IntegrityError as e:
            shortmsg = 'feed already available'
            self.logger.debug(shortmsg+str(e))
            self.lastrowid = None
            return shortmsg

    def post(self, data):
        """ feed is a dictionnary with all parts of feed
        """
        if 'url' in data:
            return self._post_one(data)
        elif 'urls' in data:
            return self._post_many(data)
        else:
            raise BadRequest('no url(s) in data')

    def _put_ok(self, feedid, feed):
        """ feed is a dictionnary with all parts of feed
        each non null part will be upgrade
        """
        matched = 3 # for 'command' and 'rssfeed_id' and 'url' which are not used below
        u = rssfeeds.update().where(rssfeeds.c.rssfeed_id == feedid)

        if 'favicon' in feed:
            favicon = feed['favicon']
            matched += 1
            u = u.values(favicon=favicon)

        if 'last_checked' in feed:
            # this is a string and not a datetime
            timestamp = feed['last_checked']
            last_checked = datetime.utcfromtimestamp(int(timestamp))
            matched += 1
            u = u.values(last_checked=last_checked)

        if 'subscribers' in feed:
            subscribers = feed['subscribers']
            matched += 1
            u = u.values(subscribers=subscribers)

        if 'title' in feed:
            title = feed['title']
            matched += 1
            u = u.values(title=title)

        if 'update_frequency' in feed:
            update_frequency = feed['update_frequency']
            matched += 1
            u = u.values(update_frequency=update_frequency)

        if 'error_count' in feed:
            error_count = feed['error_count']
            u = u.values(error_count=error_count)
            matched += 1
        else:
            error_count = 0
            u = u.values(error_count=error_count)

        if 'metadata_update' in feed:
            metadata_update = feed['metadata_update']
            u = u.values(metadata_update=metadata_update)
            matched += 1

        if 'http_status' in feed:
            http_status = feed['http_status']
            u = u.values(http_status=http_status)
            matched += 1

        if 'http_etag' in feed:
            http_etag = feed['http_etag']
            # print 'DEBUG: update etag: {0}'.format(http_etag)
            u = u.values(http_etag=http_etag)
            matched += 1

        if 'last_modified' in feed:
            timestamp = feed['last_modified']
            if timestamp is not None:
                try:
                    last_modified = dateutil.parser.parse(timestamp).astimezone(dateutil.tz.tzutc())
                    # print 'DEBUG: update lmd: {0}'.format(last_modified)
                    u = u.values(last_modified=last_modified)
                except Exception:
                    self.logger.error('Failed to parse date: {0}'.format(timestamp))
            matched += 1

        if 'compute_language' in feed:
            compute_language = feed['compute_language']
            u = u.values(compute_language=compute_language)
            matched += 1

        if 'compute_nb_items' in feed:
            compute_nb_items = feed['compute_nb_items']
            u = u.values(compute_nb_items=compute_nb_items)
            matched += 1

        if 'blacklisted' in feed:
            blacklisted = feed['blacklisted']
            u = u.values(blacklisted=blacklisted)
            matched += 1

        if len(feed) != matched:
            allowed = ['url', 'favicon', 'last_checked', 'last_modified',
                       'command', 'subscribers', 'title', 'update_frequency',
                       'error_count', 'metadata_update', 'http_status',
                       'rssfeed_id', 'http_etag', 'compute_language',
                       'compute_nb_items', 'blacklisted']
            errs = [k for k in feed if k not in allowed]
            self.logger.error('len(feed)={0} matched fields={1} field={2}'\
                              .format(len(feed), matched, errs))

        try:
            self.conn.execute(u)
        except OperationalError as oe:
            # print 'Update failed: {0}'.format(oe)
            raise BadRequest('nothing to update: {0}'.format(oe))


    def _put_error(self, feedid, feed):
        """ feed is a dictionnary with all parts of feed
        each non null part will be upgrade
        """
        u = rssfeeds.update().where(rssfeeds.c.rssfeed_id == feedid)

        if 'last_checked' in feed:
            # this is a string and not a datetime
            timestamp = feed['last_checked']
            last_checked = datetime.utcfromtimestamp(int(timestamp))
            u = u.values(last_checked=last_checked)

        if 'error_count' in feed:
            error_count = feed['error_count']+1
            u = u.values(error_count=error_count)

        if 'http_status' in feed:
            http_status = feed['http_status']
            u = u.values(http_status=http_status)

        if 'http_etag' in feed:
            http_etag = feed['http_etag']
            u = u.values(http_etag=http_etag)

        if 'blacklisted' in feed:
            blacklisted = feed['blacklisted']
            u = u.values(blacklisted=blacklisted)

        self.conn.execute(u)

    def _put_redirect(self, feedid, feed):
        """ mark feed is a redirect
        """
        r_from = feedid
        if 'redirectto' not in feed:
            raise BadRequest('redirecto is required')
        r_to = feed['redirectto']
        # feel redirect table
        ins = httpredirects.insert()\
                           .values(from_id=r_from)\
                           .values(to_id=r_to)
        try:
            self.conn.execute(ins)
        except IntegrityError as e:
            self.logger.debug('get an integrity error {0}'.format(e))
        # mark feed as a redirect
        u = rssfeeds.update().where(rssfeeds.c.rssfeed_id == feedid)

        if 'last_checked' in feed:
            # this is a string and not a datetime
            timestamp = feed['last_checked']
            last_checked = datetime.utcfromtimestamp(int(timestamp))
            u = u.values(last_checked=last_checked)

        if 'http_status' in feed:
            http_status = feed['http_status']
            u = u.values(http_status=http_status)

        if 'http_etag' in feed:
            http_etag = feed['http_etag']
            u = u.values(http_etag=http_etag)

        if 'last_modified' in feed:
            timestamp = feed['last_modified']
            try:
                last_modified = dateutil.parser.parse(timestamp).astimezone(dateutil.tz.tzutc())
                # print 'DEBUG: update lmd: {0}'.format(last_modified)
                u = u.values(last_modified=last_modified)
            except Exception:
                self.logger.error('Failed to parser date: {0}'.format(last_modified))

        self.conn.execute(u)

    def _put_deleted(self, feedid, feed):
        """ mark feed is a redirect
        """
        u = rssfeeds.update().where(rssfeeds.c.rssfeed_id == feedid)

        if 'last_checked' in feed:
            # this is a string and not a datetime
            timestamp = feed['last_checked']
            last_checked = datetime.utcfromtimestamp(int(timestamp))
            u = u.values(last_checked=last_checked)

        if 'http_status' in feed:
            http_status = feed['http_status']
            u = u.values(http_status=http_status)

        # clear etag
        u = u.values(http_etag=None)

        if 'last_modified' in feed:
            timestamp = feed['last_modified']
            last_modified = datetime.utcfromtimestamp(int(timestamp))
            u = u.values(last_modified=last_modified)

        self.conn.execute(u)

    def _put_ko(self, feedid, feed):
        """ mark feeds as KO
        """
        u = rssfeeds.update().where(rssfeeds.c.rssfeed_id == feedid)

        if 'last_checked' in feed:
            # this is a string and not a datetime
            timestamp = feed['last_checked']
            last_checked = datetime.utcfromtimestamp(int(timestamp))
            u = u.values(last_checked=last_checked)

        if 'http_status' in feed:
            http_status = feed['http_status']
            u = u.values(http_status=http_status)

        # clear etag
        u = u.values(http_etag=None)

        if 'last_modified' in feed:
            timestamp = feed['last_modified']
            last_modified = datetime.utcfromtimestamp(int(timestamp))
            u = u.values(last_modified=last_modified)

        u = u.values(error_count=rssfeeds.c.error_count+1)

        self.conn.execute(u)

    def put(self, feedid, command, feed):
        """
        dispatch put wrt command
        """
        if 'ok' in command:
            self._put_ok(feedid, feed)
        elif 'error' in command:
            self._put_error(feedid, feed)
        elif 'redirect' in command:
            self._put_redirect(feedid, feed)
        elif 'deleted' in command:
            self._put_deleted(feedid, feed)
        elif 'ko' in command:
            self._put_ko(feedid, feed)
        else:
            self.logger.info('unknown command: {0}'.format(command))
            raise BadRequest('unkown command: {0}'.format(command))

    def patch(self, feedid, command, feed):
        return self.put(feedid, command, feed)

    def delete(self, feedid):
        d = rssfeeds.delete().where(rssfeeds.c.rssfeed_id == feedid)
        self.conn.execute(d)
        return None

    def _crawl_new(self, limit):
        s = select([rssfeeds])\
            .where(rssfeeds.c.url != "")\
            .where(rssfeeds.c.http_status == 0)\
            .limit(limit)
        rows = self.conn.execute(s).fetchall()
        if rows is not None:
            return rows2dict(s, rows)
        else:
            return None

    def _crawl_error(self, limit):
        s = select([rssfeeds])\
            .where(rssfeeds.c.url != "")\
            .where(rssfeeds.c.error_count != 0)\
            .where(rssfeeds.c.http_status != 0)\
            .limit(limit)
        rows = self.conn.execute(s).fetchall()
        if rows is not None:
            return rows2dict(s, rows)
        else:
            return None

    def _crawl_withoutlanguage(self, limit):
        s = select([rssfeeds])\
            .where(rssfeeds.c.url != "")\
            .where(rssfeeds.c.http_status == 200)\
            .where(rssfeeds.c.compute_language == "??")\
            .order_by(desc(rssfeeds.c.rssfeed_id))\
            .limit(limit)
        rows = self.conn.execute(s).fetchall()
        if rows is not None:
            return rows2dict(s, rows)
        else:
            return None

    def _crawl_refresh(self, limit):
        s = select([rssfeeds])\
            .where(rssfeeds.c.url != "")\
            .where(rssfeeds.c.http_status == 200)\
            .limit(limit)
        rows = self.conn.execute(s).fetchall()
        if rows is not None:
            return rows2dict(s, rows)
        else:
            return None

    def _crawl_oldest(self, limit):
        s = select([rssfeeds])\
            .where(rssfeeds.c.url != "")\
            .where(rssfeeds.c.http_status == 200)\
            .order_by(rssfeeds.c.last_checked)\
            .limit(limit)
        rows = self.conn.execute(s).fetchall()
        if rows is not None:
            return rows2dict(s, rows)
        else:
            return None

    def crawl(self, command, limit):
        if command == 'new':
            return self._crawl_new(limit)
        elif command == 'error':
            return self._crawl_error(limit)
        elif command == 'refresh':
            return self._crawl_refresh(limit)
        elif command == 'oldest':
            return self._crawl_oldest(limit)
        elif command == 'withoutlanguage':
            return self._crawl_withoutlanguage(limit)
        else:
            raise BadRequest('unknown command {0}'.format(command))

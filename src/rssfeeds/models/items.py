#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from datetime import datetime
from sqlalchemy import Table, Column, Integer, Text, \
    UniqueConstraint, Sequence, Index, DateTime, ForeignKey, \
    select
from sqlalchemy.exc import IntegrityError

from sso.helpers import row2dict, rows2dict

from meta import rssfeeds_meta
from feeds import rssfeeds


rssitems = Table(
    'rssitems', rssfeeds_meta,
    Column('rssitem_id', Integer, Sequence('rssitems_seq'), primary_key=True),
    Column('rssfeed_id', Integer, ForeignKey(rssfeeds.c.rssfeed_id)),
    Column('published', DateTime, nullable=False),
    Column('updated', DateTime, nullable=True),
    Column('title', Text, nullable=True),
    Column('content', Text, nullable=True),
    Column('description', Text, nullable=True),
    Column('link', Text, nullable=True),
    Column('remote_id', Text, nullable=True),
    UniqueConstraint('rssfeed_id', 'published', 
                     name='rssitems_feed_published_idx'),
    UniqueConstraint('rssfeed_id', 'remote_id', 
                     name='rssitems_feed_remote_idx'),
)


Index('rssitems_idx', rssitems.c.rssfeed_id, rssitems.c.link, unique=False)


class RssItems(object):
    """ RssItems models a list of feed for a user
    # ------------------------------------------------------------------
    #
    # ------------------------------------------------------------------
    """
    def __init__(self, logger, conn):
        self.logger = logger
        self.conn = conn
        self.lastrowid = -1

    def get(self, current_rssitem_id):
        """ return a json grid with current feeds for user userid
        """
        s = select([rssitems])\
            .where(rssitems.c.rssitem_id == current_rssitem_id)
        row = self.conn.execute(s).fetchone()
        if row is None:
            return None
        else:
            return row2dict(s, row)

    def get_id(self, current_rssfeed_id, current_published):
        """ return rssitem_id from a pair feed/date
        """
        # will hit the index
        # request change for postgresql, i do not understand what
        # sqlalchemy is doing
        sel = select([rssitems])\
            .where(rssitems.c.rssfeed_id == current_rssfeed_id)\
            .where(rssitems.c.published == current_published)
        row = self.conn.execute(sel).fetchone()
        if row is not None:
            rssitem_id = row['rssitem_id']
            return rssitem_id
        else:
            return None

    def get_from_feed(self, current_rssfeed_id, offset, limit):
        """ return rssitem_id from a feed
        """
        sel = select([rssitems])\
            .where(rssitems.c.rssfeed_id == current_rssfeed_id)\
            .offset(offset).limit(limit)
        rows = self.conn.execute(sel).fetchall()
        if rows is not None:
            return rows2dict(sel, rows)
        else:
            return None

    def post(self, current_rssfeed_id, rssitem):
        """ feed is a dictionnary with all parts of feed
        """
        # does it exist already?
        if 'published' not in rssitem:
            self.logger.info('no published in item for feed {0}, skipped'\
                             .format(current_rssfeed_id))
            return None

        # check that it is really a DateTime
        timestamp = rssitem['published']
        published = datetime.utcfromtimestamp(int(timestamp))
        rssitem_id = self.get_id(current_rssfeed_id, published)
        if rssitem_id is not None:
            return self.put(rssitem_id, rssitem)

        # ok it's new
        now = datetime.utcnow()
        try:
            ins = rssitems.insert().values(rssfeed_id=current_rssfeed_id)

            ins = ins.values(published=published)

            if 'updated' in rssitem:
                # check that it is really a DateTime
                timestamp = rssitem['updated']
                updated = datetime.utcfromtimestamp(int(timestamp))
                ins = ins.values(updated=updated)
            else:
                ins = ins.values(updated=now)

            if 'title' in rssitem:
                ins = ins.values(title=rssitem['title'])

            if 'content' in rssitem:
                ins = ins.values(content=rssitem['content'])

            if 'description' in rssitem:
                ins = ins.values(description=rssitem['description'])

            if 'link' in rssitem:
                ins = ins.values(link=rssitem['link'])

            if 'remote_id' in rssitem:
                ins = ins.values(remote_id=rssitem['remote_id'])

            self.conn.execute(ins)
        except IntegrityError as e:
            self.logger.debug('get an integrity error {0}'.format(e))
            pass

    def put(self, current_rssitem_id, rssitem):
        """ item is a dictionnary with parts of item in a dict
        each non null part will be update
        """
        now = datetime.utcnow()
        try:
            u = rssitems.update()\
                        .where(rssitems.c.rssitem_id == current_rssitem_id)

            if 'rssfeed_id' in rssitem:
                u = u.values(rssfeed_id=rssitem['rssfeed_id'])

            # published can not be change

            if 'updated' in rssitem:
                # check that it is really a DateTime
                timestamp = rssitem['updated']
                updated = datetime.utcfromtimestamp(int(timestamp))
                u = u.values(updated=updated)
            else:
                u = u.values(updated=now)

            if 'title' in rssitem:
                u = u.values(title=rssitem['title'])

            if 'content' in rssitem:
                u = u.values(content=rssitem['content'])

            if 'description' in rssitem:
                u = u.values(description=rssitem['description'])

            if 'link' in rssitem:
                u = u.values(link=rssitem['link'])

            if 'remote_id' in rssitem:
                u = u.values(remote_id=rssitem['remote_id'])

            self.conn.execute(u)
        except IntegrityError as e:
            self.logger.debug('get an integrity error {0}'.format(e))

    def patch(self, feedid, feed):
        return self.put(feedid, feed)

    def delete(self, item_id):
        d = rssitems.delete().where(rssitems.c.rssitem_id == item_id)
        self.conn.execute(d)
        return None

#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from sqlalchemy import Table, Column, Integer, ForeignKey, \
    UniqueConstraint, Sequence, Index

from meta import rssfeeds_meta
from feeds import rssfeeds as table_rssfeeds


httpredirects = Table(
    'httpredirects', rssfeeds_meta,
    Column('redirect_id', Integer, Sequence('httpredirects_seq'),
           primary_key=True),
    Column('from_id', Integer, ForeignKey(table_rssfeeds.c.rssfeed_id)),
    Column('to_id', Integer, ForeignKey(table_rssfeeds.c.rssfeed_id)),
    UniqueConstraint('from_id', 'to_id', name='httpredirects_uniq')
)


Index('httpredirects_idx',
      httpredirects.c.from_id, httpredirects.c.to_id,
      unique=True)

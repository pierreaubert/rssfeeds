#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from aubergine.engine import engine2aubergine

class RssSearch(object):
    """ a class to create full text index in DB
    # ------------------------------------------------------------------
    # sqlalchemy is not really good at that
    # thus i build the index manually for
    #  = sqlite
    #  = postgresql
    #  = TODO mysql with sphynx i guess
    # ------------------------------------------------------------------
    """
    def __init__(self, logger, conn, database_url):
        self.logger = logger
        self.conn = conn
        self.lastrowid = -1
        self.ft_engine = engine2aubergine(database_url)
        self.ft_proxy = self.ft_engine(self.logger, self.conn)

    def create_tables(self):
        self.ft_proxy.add_fulltext('rssfeeds', ['url', 'title'])
        self.ft_proxy.add_fulltext('rssitems', ['title', 'content', 'description'])

    def search_feeds(self, kw):
        """ search kw in feeds """
        feeds = self.ft_proxy.search('rssfeeds', kw )
        return feeds

    def search_items(self, kw):
        """ search kw in items """
        items = self.ft_proxy.search('rssitems', kw )
        return items

    def search_url(self, kw):
        """ search an url """
        ukw = kw.split(' ').join('urls:')
        urls = self.ft_proxy.search('rssitems', ukw )
        return urls

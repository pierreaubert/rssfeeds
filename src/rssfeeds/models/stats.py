#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from datetime import datetime
import time
from sqlalchemy import Table, Column, Integer, Text, \
    Sequence, DateTime, select, func
import numpy
import json

from meta import rssfeeds_meta
from feeds import rssfeeds
from items import rssitems

stats = Table(
    'stats', rssfeeds_meta,
    Column('stats_id', Integer, Sequence('stats_seq'), primary_key=True),
    Column('stats', Text, nullable=False),
    Column('computed', DateTime, nullable=False),
)


class Stats(object):
    """ return statistics from the database
    """

    def __init__(self, logger, conn):
        self.conn = conn
        self.logger = logger
        self.lastrowid = -1

    def get(self):
        sel = select([stats.c.stats])\
            .order_by(stats.c.stats_id.desc())\
            .limit(1)
        row = self.conn.execute(sel).fetchone()
        if row is not None:
            jstats = json.loads(row['stats'])
            return jstats
        return None

    def compute(self):
        """
        """
        # compute global counters
        sql_feeds_total = select([func.count(rssfeeds.c.rssfeed_id)])
        feeds_total = self.conn.execute(sql_feeds_total).fetchone()
        self.logger.info('computed nb of feeds: {0}'.format(feeds_total[0]))

        sql_items_total = select([func.count(rssitems.c.rssitem_id)])
        items_total = self.conn.execute(sql_items_total).fetchone()
        self.logger.info('computed nb of items: {0}'.format(items_total[0]))

        # get all feeds
        # SELECT last_checked FROM rssfeeds WHERE last_checked IS NOT null;
        sql_feeds = select([rssfeeds.c.last_checked])\
            .where(rssfeeds.c.last_checked != None)
        feeds = self.conn.execute(sql_feeds).fetchall()
        tm_feeds = [time.mktime(feed[0].timetuple()) for feed in feeds]
        feeds_counts, feeds_bins = numpy.histogram(tm_feeds, bins=30)
   
        # get all items
        # SELECT published FROM rssitems WHERE published IS NOT null
        sql_items = select([rssitems.c.published])\
            .where(rssitems.c.published != None)
        items = self.conn.execute(sql_items).fetchall()
        tm_items = [time.mktime(item[0].timetuple()) for item in items]
        items_counts, items_bins = numpy.histogram(tm_items, bins=30)
   
        # compute langugage distributions
        # SELECT  compute_language AS lang, count(compute_language) AS counter
        # FROM rssfeeds WHERE http_status = 200 GROUP BY compute_language
        # ORDER BY counter DESC
        fc_cl = func.count(rssfeeds.c.compute_language)
        sql_language = select([rssfeeds.c.compute_language, fc_cl.label('counter')])\
            .where(rssfeeds.c.http_status == 200)\
            .group_by(rssfeeds.c.compute_language)\
            .order_by(fc_cl.desc())
        languages_distrib = self.conn.execute(sql_language).fetchall()

        # compute http_code distribution
        fc_hs = func.count(rssfeeds.c.http_status)
        sql_http_status = select([rssfeeds.c.http_status, fc_hs.label('counter')])\
            .group_by(rssfeeds.c.http_status)\
            .order_by(fc_hs.desc())
        http_status_distrib = self.conn.execute(sql_http_status).fetchall()

        # aggreagate all results
        results = {
            'feeds_total': feeds_total[0],
            'feeds_hist': [dict(count=count, abin=abin)
                           for count, abin in zip(feeds_counts, feeds_bins)],
            'items_total': items_total[0],
            'items_hist': [dict(count=count, abin=abin)
                           for count, abin in zip(items_counts, items_bins)],
            'languages_distrib': [dict(lg=lc[0], count=lc[1]) for lc in languages_distrib],
            'http_status_distrib': [dict(http_status=lc[0], count=lc[1]) for lc in http_status_distrib],
        }

        # cache data as json
        now = datetime.utcnow()
        ins = stats.insert().values(stats=json.dumps(results)).values(computed=now)
        self.conn.execute(ins)
        return results

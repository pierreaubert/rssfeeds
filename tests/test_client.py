#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: P. Aubert pierreaubert@yahoo.fr
#
# Apache 2 License
# ----------------------------------------------------------------------

import os
import unittest
import logging
import json
from exceptions import IOError

import httpretty

from sso.client.rest import Client as SSOClient
from rssfeeds.apprssfeeds import create_rssfeeds
from rssfeeds.client.rest import Client as RestClient

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('/tmp/{0}.log'.format(__name__))
logger.addHandler(fh)


class RestClientTests(unittest.TestCase):
    """ all tests on RssFeeds: both model and api """

    @httpretty.activate
    def setUp(self):
        """ create app """
        self.sso_url = 'http://sso'
        self.server_url = 'http://feeds'
        httpretty.register_uri(
            httpretty.POST,
            self.sso_url + '/login',
            body='{"status":"ok"}',
            status=200)
        self.server = create_rssfeeds()
        self.server.testing = True
        self.sso = SSOClient(self.sso_url, self.server.logger)
        self.client = RestClient(self.server_url,
                                 self.sso,
                                 logger)

    def tearDown(self):
        """ remove db """
        try:
            fname = self.server.config['DATABASE_URI']
            pos = fname.find('://')
            name = fname[pos+3:]
            if os.access(name, os.F_OK):
                os.unlink(name)
            else:
                print '''Can't open/close db file!'''
        except IOError as ex:
            print '''Can't open/close db file!''' + str(ex)

    @httpretty.activate
    def test_client_post_ok(self):
        httpretty.register_uri(
            httpretty.POST,
            self.server_url + '/feeds',
            body='{"status":"ok"}',
            status=200)
        assert self.client.feed_post("http://www.first.com/feed1") == 1

    @httpretty.activate
    def test_client_post_ko(self):
        httpretty.register_uri(
            httpretty.POST,
            self.server_url + '/feeds',
            body='{"staus":"ok"}',
            status=200)
        assert self.client.feed_post("http://www.first.com/feed1") == 0

    @httpretty.activate
    def test_client_put_ok(self):
        command = 'ok'
        data = {
            'rssfeed_id': 1,
        }
        httpretty.register_uri(
            httpretty.PUT,
            self.server_url + '/feeds/1?command={0}'.format(command),
            body='{"status":"ok"}',
            status=200)
        assert self.client.feed_put(command, data) == 1

    @httpretty.activate
    def test_client_get_id(self):
        url = 'http://test.com'
        body = {
            'status': 'ok',
            'feed': {
                'rssfeed_id': 1
            }
        }
        httpretty.register_uri(
            httpretty.GET,
            self.server_url + '/search/urls/',
            body=json.dumps(body),
            status=200)
        assert self.client.feed_get_id(url) == 1


if __name__ == '__main__':
    unittest.main()


# []
# {}

#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: P. Aubert pierreaubert@yahoo.fr
#
# Apache 2 License
# ----------------------------------------------------------------------

import os
import time
from datetime import datetime
import unittest
import json
import httpretty
from exceptions import IOError
from contextlib import contextmanager
from flask import appcontext_pushed, g

from sso.accesscontrol.ac import AC_User
from rssfeeds.apprssfeeds import create_rssfeeds

# a few horrible global variables
# add a fake cookie
cookie = ('Cookie', 'ssosign=ok; Expires=Wed, 29-Jan-2014 09:08:08 GMT; Max-Age=900; Path=/')
contenttype = ('Content-Type', 'application/json')
common_headers = [ cookie, contenttype ]
sso_url = 'http://127.0.0.1:9090' 


@contextmanager
def mock_set_user(app, user_id):
    """ create a fake user_id
    here we bypass the check in sso.client.decorators.dflask
    """
    def handler(sender, **kwargs):
        """ store the fake id in g """
        g.userinfo = {
            'user_id': user_id
        }
        g.rules = {}
        if user_id == 1:
            g.userinfo['username'] = 'admin'
            g.rules[str(AC_User('admin'))] = True
        else:
            g.userinfo['username'] = 'crawler'
            g.rules[str(AC_User('crawler'))] = True
    with appcontext_pushed.connected_to(handler, app):
        yield


# -----------------------------------------------------------------------------
#
# test auth via fake auth
#
# -----------------------------------------------------------------------------
class RssFeedsNoAuthTests(unittest.TestCase):
    """ test that you can't access /methods without a valid auth cookie """

    def setUp(self):
        """ create app """
        self.server = create_rssfeeds()
        self.server.testing = True

    def tearDown(self):
        """ remove db """
        try:
            fname = self.server.config['DATABASE_URI']
            pos = fname.find('://')
            name = fname[pos+3:]
            if os.access(name, os.F_OK):
                os.unlink(name)
            else:
                print '''Can't open/close db file!'''
        except IOError as ex:
            print '''Can't open/close db file!''' + str(ex)

    def test_empty_db(self):
        """ let see if the schema as been uploaded
            and check that we can not access a method
        """
        with self.server.test_client() as tc:
            jstats = tc.get('/stats')
            self.assertEqual(jstats.status_code, 401)


# ---------------------------------------------------------------------------
#
#                          DEFINE ALL FEEDS STEPS
#
# ---------------------------------------------------------------------------

# ---------------------------------------------------------------------------
# POST
# ---------------------------------------------------------------------------
def step_post_feed(tc, id, data):
    """ post a feed with data, expected db id is id """
    jdata = json.dumps(data)
    jresp = tc.post('/feeds', data=jdata, headers=common_headers)
    assert jresp.status_code == 200
    resp = json.loads(jresp.data)
    assert 'status' in resp
    assert resp['status'] == 'ok'
    assert 'rssfeed_id' in resp
    # this is a rowid and may not be 1 in all db
    # cassandra will return a UUID (and PG could too)
    assert resp['rssfeed_id'] == id
    return data


def step_post_feed1(tc):
    """ post first feed """
    data = {
        'url': 'http://www.nice.com/feed1.rss'
    }
    return step_post_feed(tc, 1, data)


def step_post_feed2(tc):
    """ post first feed """
    data = {
        'url': 'http://www.notnice.com/feed2.rss'
    }
    return step_post_feed(tc, 2, data)


def step_post_feed_withouturl(tc):
    """ try to post without a url """
    empty = {
        'http': 'http://www.nice.com/feed1.rss'
    }
    jempty = json.dumps(empty)
    jresp = tc.post('/feeds', data=jempty, headers=common_headers)
    assert jresp.status_code == 400
    resp = json.loads(jresp.data)
    assert 'status' in resp
    assert resp['status'] == 'ko'


def step_post_feed1_secondtime(tc, data):
    """ try to post again to see if we get an error """
    jdata = json.dumps(data)
    jresp = tc.post('/feeds', data=jdata, headers=common_headers)
    assert jresp.status_code == 200
    resp = json.loads(jresp.data)
    assert 'status' in resp
    assert resp['status'] == 'ko'
    assert 'error' in resp
    assert 'feed already available' in resp['error']


# ---------------------------------------------------------------------------
# GET
# ---------------------------------------------------------------------------
def step_get_feed(tc, rssfeed_id, data):
    """ get first feed """
    jresp = tc.get('/feeds/{0}'.format(rssfeed_id),
                   headers=common_headers)
    assert jresp.status_code == 200
    resp = json.loads(jresp.data)
    assert 'status' in resp
    assert resp['status'] == 'ok'
    assert 'feed' in resp
    feed = resp['feed']
    assert 'url' in feed
    assert feed['url'] == data['url']


def step_get_feed_checkcontent(tc, rssfeed_id, data):
    """ do we get back what we put inside? """
    jresp = tc.get('/feeds/{0}'.format(rssfeed_id),
                   headers=common_headers)
    assert jresp.status_code == 200
    resp = json.loads(jresp.data)
    assert 'status' in resp
    assert resp['status'] == 'ok'
    assert 'feed' in resp
    feed = resp['feed']
    fields = ['url', 'favicon', 'title', 'subscribers',
              'update_frequency', 'error_count', 'metadata_update',
              'http_status', 'http_etag', 'compute_language',
              'compute_nb_items', 'blacklisted']
    for field in fields:
        # print field
        assert field in data
        assert field in feed
        assert feed[field] == data[field]
        datefields = ['last_checked', 'last_modified']
        for field in datefields:
            assert field in feed
            #print 'Feed='+feed[field]
            #print 'Data='+str(data[field])
            # find the good check, need to compare 2 timestamps both utc
            # assert feed[field] == data[field]


def step_get_feeds(tc, rssfeeds_id):
    """ get all feed """
    jresp = tc.get('/feeds', headers=common_headers)
    assert jresp.status_code == 200
    resp = json.loads(jresp.data)
    assert 'status' in resp
    assert resp['status'] == 'ok'
    assert 'feed' not in resp
    assert 'feeds' in resp
    feeds = resp['feeds']
    for feed in feeds:
        assert 'url' in feed


# ---------------------------------------------------------------------------
# PUT
# ---------------------------------------------------------------------------
def step_put_data_into_feed(tc, command, rssfeed_id, data):
    jdata = json.dumps(data)
    jresp = tc.put('/feeds/{0}?command={1}'.format(rssfeed_id, command),
                   data=jdata,
                   headers=common_headers)
    assert jresp.status_code == 200
    resp = json.loads(jresp.data)
    assert 'status' in resp
    assert resp['status'] == 'ok'
    return data


def step_put_data1_into_feed1(tc):
    """ try to out more datas into feed """
    data = {
        'rssfeed_id': 1,
        'published': time.mktime(datetime.utcnow().timetuple()),
        'title': 'title 1',
        'remote_id': 'feed1'
    }
    return step_put_data_into_feed(tc, 'ok', 1, data)


def step_put_data2_into_feed1(tc):
    """ try to out more datas into feed """
    data = {
        'url': 'http://www.nice.com/feed1.rss',
        'favicon': 'http://www.nice.com/favicon.ico',
        'rssfeed_id': 1,
        'title': 'title 1',
        'last_checked': time.mktime(datetime.utcnow().timetuple()),
        'subscribers': 1,
        'update_frequency': 3,
        'error_count': 0,
        'http_status': 200,
        'http_etag': 'XOIUOISDN34LNJ34',
        'last_modified': time.mktime(datetime.utcnow().timetuple()),
        'compute_language': 'en',
        'compute_nb_items': 123,
        'blacklisted': False,
        'metadata_update': 3
    }
    return step_put_data_into_feed(tc, 'ok', 1, data)


def step_put_data_into_feed2(tc):
    """ try to out more datas into feed """
    data = {
        'url': 'http://www.notnice.com/feed2.rss',
        'favicon': 'http://www.nice.com/favicon.ico',
        'rssfeed_id': 2,
        'title': 'title 2',
        'last_checked': time.mktime(datetime.utcnow().timetuple()),
        'subscribers': 1,
        'update_frequency': 3,
        'error_count': 0,
        'http_status': 200,
        'http_etag': 'XOIUOISDN34LNJ34',
        'last_modified': time.mktime(datetime.utcnow().timetuple()),
        'compute_language': 'en',
        'compute_nb_items': 123,
        'blacklisted': False,
        'metadata_update': 3
    }
    return step_put_data_into_feed(tc, 'ok', 2, data)


def step_put_http_5xx_data1_into_feed1(tc, error_count):
    """ put feed in error """
    data = {
        'rssfeed_id': 1,
        'last_checked': time.mktime(datetime.utcnow().timetuple()),
        'http_status': 500,
        'error_count': error_count,
    }
    return step_put_data_into_feed(tc, 'error', 1, data)


def step_put_http_4xx_data1_into_feed1(tc):
    """ put feed in error """
    data = {
        'rssfeed_id': 1,
        'last_checked': time.mktime(datetime.utcnow().timetuple()),
        'http_status': 400,
    }
    return step_put_data_into_feed(tc, 'error', 1, data)


def step_put_http_3xx_data1_into_feed1(tc, url):
    """ redirect """
    data = {
        'rssfeed_id': 1,
        'last_checked': time.mktime(datetime.utcnow().timetuple()),
        'http_status': 302,
        'redirectto': 2,
        'redirecttourl': url,
    }
    return step_put_data_into_feed(tc, 'redirect', 1, data)


# ---------------------------------------------------------------------------
# PATCH
# ---------------------------------------------------------------------------
def step_patch_data_into_feed(tc, rssfeed_id, data):
    jdata = json.dumps(data)
    jresp = tc.patch('/feeds/{0}'.format(rssfeed_id),
                     data=jdata,
                     headers=common_headers)
    assert jresp.status_code == 200
    resp = json.loads(jresp.data)
    assert 'status' in resp
    assert resp['status'] == 'ok'
    return data


def step_patch_data3_into_feed1(tc):
    """ try to out more datas into feed """
    data = {
        'url': 'http://www.nice.com/feed1.rss',
        'favicon': 'http://www.nice.com/favicon.ico',
        'rssfeed_id': 1,
        'title': 'title 1',
        'last_checked': time.mktime(datetime.utcnow().timetuple()),
        'subscribers': 1,
        'update_frequency': 4,
        'error_count': 0,
        'http_status': 302,
        'http_etag': 'XOIUOISDN34LNJ34',
        'last_modified': time.mktime(datetime.utcnow().timetuple()),
        'compute_language': 'en',
        'compute_nb_items': 124,
        'blacklisted': False,
        'metadata_update': 3
    }
    return step_patch_data_into_feed(tc, 1, data)


# ---------------------------------------------------------------------------
# DELETE
# ---------------------------------------------------------------------------
def step_delete_feed(tc, id):
    """ can we delete it """
    jresp = tc.delete('/feeds/{0}'.format(id),
                      headers=common_headers)
    assert jresp.status_code == 403

# ---------------------------------------------------------------------------
# Monkey Patch
# ---------------------------------------------------------------------------
def monkeypatch_enable():
        # monkey patch the call to /accessrule
        httpretty.enable()
        # first call faked cookies check
        httpretty.register_uri(
            httpretty.GET,
            sso_url + '/profile',
            body='{"status":"ok", "users": {"user_id":1, "username": "admin"}}',
            status=200)
        httpretty.register_uri(
            httpretty.POST,
            sso_url + '/accessrules',
            body='{"status":"ok"}',
            status=200)

def monkeypatch_disable():    
        # revert monkey patch
        httpretty.disable()
        httpretty.reset()

# -----------------------------------------------------------------------------
#
# test RssFeeds
#
# -----------------------------------------------------------------------------
class RssFeedsTests(unittest.TestCase):
    """ all tests on RssFeeds: both model and api """

    def setUp(self):
        """ create app """
        self.server = create_rssfeeds()
        self.server.testing = True
        monkeypatch_enable()

    def tearDown(self):
        """ remove db """
        monkeypatch_disable()
        # try to remove db file
        try:
            fname = self.server.config['DATABASE_URI']
            pos = fname.find('://')
            name = fname[pos+3:]
            if os.access(name, os.F_OK):
                os.unlink(name)
            else:
                print '''Can't open/close db file!'''
        except IOError as ex:
            print '''Can't open/close db file!''' + str(ex)

    def test_fake_authent(self):
        """ let see if the schema as been uploaded
        and that we can acces the DB with the proper authent
        """
        # log user 1 via faking context
        with mock_set_user(self.server, 1):
            with self.server.test_client() as tc:
                jstats = tc.get('/stats')
                self.assertEqual(jstats.status_code, 200)

    # -------------------------------------------------------------------------
    # CHECK ALL UC
    # -------------------------------------------------------------------------
    def test_create_feed1(self):
        """ create a feed and see if it is store correctly """
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                data1 = step_post_feed1(tc)
                step_get_feed(tc, 1, data1)

    def test_create_wrongfeed1(self):
        """ create a feed and see if it is rejected """
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                step_post_feed_withouturl(tc)

    def test_put_ok_feed1(self):
        """ create a feed and see if it is store corectly """
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                step_post_feed1(tc)
                step_put_data1_into_feed1(tc)
                data2 = step_put_data2_into_feed1(tc)
                step_get_feed_checkcontent(tc, 1, data2)

    def test_put_wrong_command(self):
        """ create a feed and see if it is store corectly """
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                data = {}
                jdata = json.dumps(data)
                jresp = tc.put('/feeds/1?command=zzz', 
                               data=jdata,
                               headers=common_headers)
                assert jresp.status_code == 400
                resp = json.loads(jresp.data)
                assert 'status' in resp
                assert resp['status'] == 'ko'
                assert 'error' in resp
                assert 'unkown' in resp['error']
                assert 'zzz' in resp['error']

    def test_put_http_4xx_feed1(self):
        """ create a feed, get a 4XX http error """
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                step_post_feed1(tc)
                step_put_http_4xx_data1_into_feed1(tc)
                # so what append?
                jresp = tc.get('/feeds/1', headers=common_headers)
                assert jresp.status_code == 200
                resp = json.loads(jresp.data)
                assert 'status' in resp
                assert resp['status'] == 'ok'
                assert 'feed' in resp
                feed = resp['feed']
                assert 'error_count' in feed
                assert feed['error_count'] == 0
                assert 'http_status' in feed
                http_status = feed['http_status']
                assert http_status >= 400 and http_status < 500

    def test_put_http_5xx_feed1(self):
        """ create a feed and get a 5xx http errror """
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                step_post_feed1(tc)
                for i in range(1, 3):
                    data = step_put_http_5xx_data1_into_feed1(tc, i)
                    # so what append?
                    jresp = tc.get('/feeds/1', headers=common_headers)
                    assert jresp.status_code == 200
                    resp = json.loads(jresp.data)
                    assert 'status' in resp
                    assert resp['status'] == 'ok'
                    assert 'feed' in resp
                    feed = resp['feed']
                    assert 'error_count' in feed
                    assert 'error_count' in data
                    # it expect error_count to be increased
                    assert feed['error_count'] == (data['error_count']+1)
                    assert 'http_status' in feed
                    http_status = feed['http_status']
                    assert http_status >= 500 and http_status < 600

    def test_put_http_304_feed1(self):
        """ create a feed and get a 3xx http code """
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                data1 = step_post_feed1(tc)
                # data2 = step_post_feed2(tc)
                # data3 = step_put_http_3xx_data1_into_feed1(tc, data1['url'])
                # data1 now should point to data2
                jresp = tc.get('/feeds/1', headers=common_headers)
                assert jresp.status_code == 200
                resp = json.loads(jresp.data)
                assert 'status' in resp
                assert resp['status'] == 'ok'
                assert 'feed' in resp
                feed = resp['feed']
                assert 'error_count' in feed
                assert feed['error_count'] == 0
                assert 'http_status' in feed
                http_status = feed['http_status']
                assert http_status >= 300 and http_status < 400

    def test_patch_feed1(self):
        """ create a feed and see if it is store corectly """
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                step_post_feed1(tc)
                step_put_data1_into_feed1(tc)
                data3 = step_patch_data3_into_feed1(tc)
                step_get_feed_checkcontent(tc, 1, data3)

# failed but normal because of too heavy monkey patch
#    def test_delete_feed1(self):
#        with mock_set_user(self.server, 2):
#            with self.server.test_client() as tc:
#                data1 = step_post_feed1(tc)
#                step_delete_feed(tc, 1)
#                step_get_feed(tc, 1, data1)

    def test_get_feeds(self):
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                step_post_feed1(tc)
                step_post_feed2(tc)
                step_get_feeds(tc, [1, 2])

    def test_crawl_apis_new(self):
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                data1 = step_post_feed1(tc)
                data2 = step_post_feed2(tc)
                jresp = tc.get('/crawl/new?limit=3', headers=common_headers)
                assert jresp.status_code == 200
                resp = json.loads(jresp.data)
                assert 'status' in resp
                assert resp['status'] == 'ok'
                assert 'feed' not in resp
                assert 'feeds' in resp
                feed1 = resp['feeds'][0]
                feed2 = resp['feeds'][1]
                assert data1['url'] == feed1['url']
                assert data2['url'] == feed2['url']
                

# ---------------------------------------------------------------------------
#
#                          DEFINE ALL ITEMS STEPS
#
# ---------------------------------------------------------------------------


# ---------------------------------------------------------------------------
# POST
# ---------------------------------------------------------------------------
def step_post_feed_item(tc, feed_id, data):
    jdata = json.dumps(data)
    jresp = tc.post('/items', data=jdata, headers=common_headers)
    print jresp
    assert jresp.status_code == 200
    return data


def step_post_feed_item1(tc, feed_id):
    data = {
        'rssfeed_id': feed_id,
        'title': 'item 1 in feed1 is not intresting',
        'content': 'content 1 in feed1 is not long',
        'description': 'description 1 in feed1 is not long but a bit \
        longer than content 1 it is on purpose for checking that \
        search functions work',
        'published': time.mktime(datetime.utcnow().timetuple()),
        'updated': time.mktime(datetime.utcnow().timetuple()),
        'link': 'http://www.nice.com/feed1/item1.xml',
        'remote_id': 'feed_1_item_1'
    }
    return step_post_feed_item(tc, 1, data)


def step_post_feed_item2(tc, feed_id):
    data = {
        'rssfeed_id': feed_id,
        'title': 'item 2 in feed1 is not intresting',
        'content': 'content 2 in feed1 is not long',
        'description': 'description 2 in feed1 is not long but a bit \
        longer than content 2 it is on purpose for checking that \
        search functions work',
        'published': time.mktime(datetime.utcnow().timetuple()),
        'updated': time.mktime(datetime.utcnow().timetuple()),
        'link': 'http://www.nice.com/feed1/item2.xml',
        'remote_id': 'feed_1_item_2'
    }
    return step_post_feed_item(tc, 1, data)


# ---------------------------------------------------------------------------
# PUT
# ---------------------------------------------------------------------------
def step_put_feed_item1(tc, feed_id, item_id):
    data = {
        'rssfeed_id': feed_id,
        'title': 'item 1 in feed1 is not intresting but can be patched',
        'content': 'content 1 in feed1 as been patched',
        'description': 'description 1 in feed1 is not long but a bit \
        longer than content 1 it is on purpose for checking that \
        search functions work and that i can patch it',
        'link': 'http://www.nice.com/feed1/item1-patched.xml',
        'remote_id': 'feed_1_item_1_patched'
    }
    jdata = json.dumps(data)
    jresp = tc.put('/items/{0}'.format(item_id),
                   data=jdata,
                   headers=common_headers)
    assert jresp.status_code == 200
    return data


# ---------------------------------------------------------------------------
# GET
# ---------------------------------------------------------------------------
def step_get_item(tc, item_id, data):
    jresp = tc.get('/items/{0}'.format(item_id),
                   headers=common_headers)
    assert jresp.status_code == 200
    resp = json.loads(jresp.data)
    assert 'status' in resp
    assert resp['status'] == 'ok'
    # print resp
    assert 'item' in resp
    feed = resp['item']
    fields = ['title', 'content', 'description', 'link', 'remote_id']
    for field in fields:
        # print field
        assert field in data
        assert field in feed
        assert feed[field] == data[field]
    datefields = ['published', 'updated']
    for field in datefields:
        assert field in feed
        # print 'Feed='+feed[field]
        # print 'Data='+str(data[field])
        # find the good check, need to compare 2 timestamps both utc
        # assert feed[field] == data[field]


# ---------------------------------------------------------------------------
# DELETE
# ---------------------------------------------------------------------------
def step_delete_item(tc, item_id):
    jresp = tc.delete('/items/{0}'.format(item_id),
                      headers=common_headers)
    assert jresp.status_code == 200
    resp = json.loads(jresp.data)
    assert 'status' in resp
    assert resp['status'] == 'ok'


# -----------------------------------------------------------------------------
#
# all test on items
#
# -----------------------------------------------------------------------------
class ItemFeedsTests(unittest.TestCase):
    """ all tests on RssFeeds: both model and api """

    def setUp(self):
        """ create app """
        self.server = create_rssfeeds()
        self.server.testing = True
        self.headers = common_headers
        monkeypatch_enable()

    def tearDown(self):
        """ remove db """
        monkeypatch_disable()
        try:
            fname = self.server.config['DATABASE_URI']
            pos = fname.find('://')
            name = fname[pos+3:]
            if os.access(name, os.F_OK):
                os.unlink(name)
            else:
                print '''Can't open/close db file!'''
        except IOError as ex:
            print '''Can't open/close db file!''' + str(ex)

    # -------------------------------------------------------------------------
    # CHECK ALL UC
    # -------------------------------------------------------------------------
    def test_create_feed1_items(self):
        """ create a feed and add some items """
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                step_post_feed1(tc)
                data1 = step_post_feed_item1(tc, 1)
                step_get_item(tc, 1, data1)

    def test_put_feed1_items(self):
        """ create a feed, patch it and check result """
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                step_post_feed1(tc)
                step_post_feed_item1(tc, 1)
                data1 = step_put_feed_item1(tc, 1, 1)
                step_get_item(tc, 1, data1)

    def test_delete_feed1_items(self):
        """ create a feed+items, patch it and delete it """
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                step_post_feed1(tc)
                step_post_feed_item1(tc, 1)
                step_post_feed_item2(tc, 1)

        with mock_set_user(self.server, 1):
            with self.server.test_client() as tc:
                step_delete_item(tc, 1)
                step_delete_item(tc, 2)


# -----------------------------------------------------------------------------
#
# test Stats
#
# -----------------------------------------------------------------------------
class StatsTests(unittest.TestCase):
    """ all tests on RssFeeds: both model and api """

    def setUp(self):
        """ create app """
        self.server = create_rssfeeds()
        self.server.testing = True
        self.headers = common_headers
        monkeypatch_enable()

    def tearDown(self):
        """ remove db """
        monkeypatch_disable()
        try:
            fname = self.server.config['DATABASE_URI']
            pos = fname.find('://')
            name = fname[pos+3:]
            if os.access(name, os.F_OK):
                os.unlink(name)
            else:
                print '''Can't open/close db file!'''
        except IOError as ex:
            print '''Can't open/close db file!''' + str(ex)

    def test_stats_get(self):
        """ test get on stats """
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                step_post_feed1(tc)
                step_put_data1_into_feed1(tc)
                step_put_data2_into_feed1(tc)
                step_post_feed2(tc)
                step_put_data_into_feed2(tc)
                jstats = tc.get('/stats')
                self.assertEqual(jstats.status_code, 200)
                stats = json.loads(jstats.data)
                assert 'status' in stats
                assert stats['status'] == 'ok'


# PA BUGS
#    def test_stats_patch(self):
#        """ test get on stats """
#        with mock_set_user(self.server, 2):
#            with self.server.test_client() as tc:
#                step_post_feed1(tc)
#                step_put_data1_into_feed1(tc)
#                step_put_data2_into_feed1(tc)
#                step_post_feed2(tc)
#                step_put_data_into_feed2(tc)
#
#        with mock_set_user(self.server, 1):
#            with self.server.test_client() as tc:
#                # compute
#                jstats = tc.patch('/stats')
#                self.assertEqual(jstats.status_code, 200)
#                stats = json.loads(jstats.data)
#                assert 'status' in stats
#                assert stats['status'] == 'ok'
#
#        with mock_set_user(self.server, 1):
#            with self.server.test_client() as tc:
#                # get results
#                jstats = tc.get('/stats')
#                self.assertEqual(jstats.status_code, 200)
#                stats = json.loads(jstats.data)
#                assert 'status' in stats
#                assert stats['status'] == 'ok'
#                print 'stats'
#                print stats


# -----------------------------------------------------------------------------
#
# test search on RssFeeds
#
# -----------------------------------------------------------------------------
class RssFeedsSearchTests(unittest.TestCase):
    """ all search tests on RssFeeds: both model and api """

    def setUp(self):
        """ create app """
        self.server = create_rssfeeds()
        self.server.testing = True
        monkeypatch_enable()

    def tearDown(self):
        """ remove db """
        monkeypatch_disable()
        # try to remove db file
        try:
            fname = self.server.config['DATABASE_URI']
            pos = fname.find('://')
            name = fname[pos+3:]
            if os.access(name, os.F_OK):
                os.unlink(name)
            else:
                print '''Can't open/close db file!'''
        except IOError as ex:
            print '''Can't open/close db file!''' + str(ex)

    # -------------------------------------------------------------------------
    # CHECK ALL UC
    # -------------------------------------------------------------------------
    def test_search_feeds(self):
        """ create a feed and see if it is store correctly """
        with mock_set_user(self.server, 2):
            with self.server.test_client() as tc:
                # create 2 feeds
                step_post_feed1(tc)
                step_post_feed2(tc)
                # search
                


if __name__ == '__main__':
    unittest.main()

# stupid keyboard
# [] {} / ~ ()

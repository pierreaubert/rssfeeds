#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from rssfeeds.apprssfeeds import create_rssfeeds

# create the application for WSGI server
app = create_rssfeeds()

# nice for debugging
if __name__ == '__main__':
    app.run(host=app.config['RSSFEEDS_HOST'],
            port=app.config['RSSFEEDS_PORT'],
            debug=app.config['DEBUG'])
